package main

import (
	mergelab "gitlab.com/mergetb/facilities/mergelab/model"
	"gitlab.com/mergetb/xir/v0.3/go/build"
)

func main() {
	build.Run(mergelab.Topo())
}

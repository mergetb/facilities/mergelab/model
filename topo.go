package mergelab

import (
	"fmt"
	"strings"
	"strconv"

        log "github.com/sirupsen/logrus"

	xir "gitlab.com/mergetb/xir/v0.3/go"
	. "gitlab.com/mergetb/xir/v0.3/go/build"
)

var (
	ifr   *xir.Resource
	ops   *xir.Resource
	nodes []*xir.Resource
	mgmt  *xir.Resource
	//infra *xir.Resource
	ixp    *xir.Resource
)

func assignVFMacs() {
       for _, x := range nodes {
               for ni, n := range x.NICs {
                       var pfPort *xir.Port = nil
                       for pi, p := range n.Ports {
                               if p.Mac != "" {
                                       continue
                               }
                               if p.Sriov == nil || p.Sriov.GetVf() == nil {
                                       continue
                               }
                               pfIndex := p.Sriov.PfIndex
                               if pfPort == nil || pfPort.Index != pfIndex {
                                       for _, pp := range n.Ports {
                                               if pp.Index == pfIndex {
                                                       pfPort = pp
                                                       break
                                               }
                                       }
                               }
                               if pfPort.Mac == "" {
                                       log.Warnf("Unassigned pfport %s, nic: %d, port %d", x.Id, ni, pi)
                                       continue
                               }
                               MacBytes := strings.Split(pfPort.Mac, ":")
                               lastByte, err := strconv.ParseUint(MacBytes[5], 16, 8)
                               if err != nil {
                                       panic("assignVFMacs failed on mac " + pfPort.Mac)
                               }
                               MacBytes[3] = "00"
                               MacBytes[5] = fmt.Sprintf("%02x", uint8(lastByte) + uint8(p.Index - pfIndex))
                               p.Mac = strings.Join(MacBytes, ":")
                       }
               }
       }
}

func Topo() *xir.Facility {

	tb, err := NewBuilder("mergelab", "facility.mergelab.net")
	if err != nil {
		log.Fatalf("new builder: %v", err)
	}

	// Infrastructure server
	ifr = tb.Infraserver("ifr",
		Procs(1, Cores(8), Product("Intel", "Xeon CPU E31230 @ 3.20GHz")),
		Dimms(2, GB(4), Product("Kingston", "DDR3 DIMM", "9965525-010.A00LF")),
		HDDs(1, GB(930), Sata3(), SysDisk(), Product("Western Digital", "HDD", "WDC WD1003FBYX-0")),
		HDDs(1, GB(930), Sata3(), EtcdDisk(), Product("Western Digital", "HDD", "WDC WD1003FBYX-0")),
		HDDs(1, GB(930), Sata3(), MinioDisk(), Product("Western Digital", "HDD", "WDC WD1003FBYX-0")),
		Eno(2, Gbps(1), Gw(0), Mgmt(1)),
		Enp(4, 1, Gbps(10), Infranet()),
		Rootdev("/dev/sda"),
		RoleAdd(xir.Role_BorderGateway),
		RoleAdd(xir.Role_Gateway),
		RoleAdd(xir.Role_EtcdHost),
		RoleAdd(xir.Role_MinIOHost),
		RoleAdd(xir.Role_SledHost),
		RoleAdd(xir.Role_InfrapodServer),
		Bios(),
		Product("Unknown", "Harvested Deter MiniBed Machine", ""),
	)
	ifr.Mgmt().Mac = "00:1e:67:19:58:89"
	ifr.NICs[1].Ports[0].Name = "ens261f0"
	ifr.NICs[1].Ports[0].Mac = "3c:fd:fe:9e:f0:28"
	ifr.NICs[1].Ports[1].Name = "ens261f1"
	ifr.NICs[1].Ports[1].Mac = "3c:fd:fe:9e:f0:29"
	ifr.NICs[1].Ports[2].Name = "ens261f2"
	ifr.NICs[1].Ports[2].Mac = "3c:fd:fe:9e:f0:2a"
	ifr.NICs[1].Ports[3].Name = "ens261f3"
	ifr.NICs[1].Ports[3].Mac = "3c:fd:fe:9e:f0:2b"

	// Ops server
	ops = tb.OpsServer("ops",
		Procs(1, Cores(8), Product("Intel", "Xeon CPU E31230 @ 3.20GHz")),
		Dimms(2, GB(4), Product("Kingston", "DDR3 DIMM", "9965525-010.A00LF")),
		HDDs(2, GB(930), Product("Western Digital", "HDD", "WDC WD1003FBYX-0")),
		Eth(1, Gbps(1)),
		Eno(1, Gbps(1)),
		Enp(4, 1, Gbps(1), Mgmt(2), Product("Intel Corporation", "I350 Gigabit Network Connection", "X710")),
		Bios(),
		Product("Unknown", "Harvested Deter MiniBed Machine", ""),
	)
	ops.Mgmt().Mac = "a0:36:9f:02:58:03"

	// switches
	mgmt = tb.MgmtLeaf("mgmt",
		Eth(1, Gbps(1), Mgmt()),
		Swp(8, Gbps(1), Mgmt()),
		Product("NetGear", "1 Gbps unmanaged switch", ""),
	)

	/*
	infra = tb.InfraLeaf("infra",
		Eth(1, Gbps(1), Mgmt()),
		Swp(48, Gbps(1), Infranet()),
		Swp(4, Gbps(10), Infranet()),
		Product("QuantaMesh Networking", "BMS T1048 48x10/100/100BASE-T+4x10GbE SFP+", "T1048-LY4R 1LY4BZZ0STH"),
	)
	infra.Mgmt().Mac = "b4:a9:fc:a7:07:02"
	*/

	ixp = tb.InfraLeaf("ixp",
		Eth(1, Gbps(1), Mgmt()),
		Swp(8, Gbps(100), Infranet()),
		Swp(8, Gbps(100), Xpnet()),
		Product("Mellanox", "SN2100C 16x100GbE", "MSN2100-CB2F"),
		RoleAdd(xir.Role_Stem),
		RoleAdd(xir.Role_XpSwitch), // xpleaf and infraleaf all in one
	)
	ixp.Mgmt().Mac = "0c:42:a1:36:b3:e8"

	nodes = tb.Nodes(2, "x",
		Procs(1, Cores(32), Product("AMD", "EPYC 7502P 32-Core @ 2500 MHz")),
		Dimms(8, GB(32)),
		NVMEs(2, 0, GB(2000), MarinerDisk()),
		NVMEs(1, 2, GB(960), SysDisk()),
		NVMEs(1, 3, GB(960)),
		BMCIpmi(), Uefi(),
		AllocModes(xir.AllocMode_Physical, xir.AllocMode_Virtual),
		Roles(xir.Role_TbNode, xir.Role_Hypervisor),
		Ipmi(1, Gbps(1)),
		Eno(2, Gbps(1), Mgmt(0)),
		Enp(1, 1, Gbps(50), Infranet(0)), // connect-x5
		Enp(2, 1, PortNames("enp3s0f0np0", "enp3s0f1np1"), Gbps(100), Xpnet(), VFs(10)), // connect-x6
		Rootdev("/dev/nvme2n1"),
		DefaultImage("bullseye"),
	)

	nodes[0].NICs[0].Ports[0].Mac = "b4:2e:99:f7:cb:6c"
	nodes[0].Mgmt().Mac = "b4:2e:99:f7:cb:6a"
	nodes[0].Infranet().Mac = "0c:42:a1:42:b0:26"
	nodes[0].Infranet().Name = "enp66s0np0"
	nodes[0].NICs[3].Ports[0].Mac = "0c:42:a1:2c:e4:ba"
	nodes[0].NICs[3].Ports[1].Mac = "0c:42:a1:2c:e4:bb"

	nodes[1].NICs[0].Ports[0].Mac = "b4:2e:99:f7:cb:70"
	nodes[1].Mgmt().Mac = "b4:2e:99:f7:cb:6e"
	nodes[1].Infranet().Mac = "0c:42:a1:42:b0:1a"
	nodes[1].Infranet().Name = "enp66s0np0"
	nodes[1].NICs[3].Ports[0].Mac = "0c:42:a1:98:55:20"
	nodes[1].NICs[3].Ports[1].Mac = "0c:42:a1:98:55:21"

	// mgmt
	tb.Connect(ixp.Mgmt(), mgmt.NextSwpG(1), RJ45())
	tb.Connect(ops.Mgmt(), mgmt.NextSwpG(1), RJ45())
	tb.Connect(ifr.Mgmt(), mgmt.NextSwpG(1), RJ45())
	tb.Connect(nodes[0].Mgmt(), mgmt.NextSwpG(1), RJ45())
	tb.Connect(nodes[1].Mgmt(), mgmt.NextSwpG(1), RJ45())

	// infra
	tb.BreakoutTrunk(
		ixp.NICs[1],
		ixp.SwpIndex(1),
		ifr.NICs[1].Ports,
		"ifr",
		"ixp",
		DACBreakout(),
	)

	// DAC 100G
	tb.Connect(nodes[0].Infranet(), ixp.SwpIndex(7))
	tb.Connect(nodes[1].Infranet(), ixp.SwpIndex(8))

	// xp
	// DAC 40 Gbps
	tb.Connect(nodes[0].NICs[3].Ports[0], ixp.SwpIndex(9))
	tb.Connect(nodes[0].NICs[3].Ports[1], ixp.SwpIndex(10))
	tb.Connect(nodes[1].NICs[3].Ports[0], ixp.SwpIndex(11))
	tb.Connect(nodes[1].NICs[3].Ports[1], ixp.SwpIndex(12))

	/*
	tb.Breakout(
		ixp.NICs[2],
		ixp.SwpIndex(9),
		[]*xir.Port{
			nodes[0].NICs[3].Ports[1], nodes[0].NICs[3].Ports[0],
			nodes[1].NICs[3].Ports[1], nodes[1].NICs[3].Ports[0],
		},
		DACBreakout(),
	)
	*/

	assignVFMacs()
	return tb.Facility()
}
